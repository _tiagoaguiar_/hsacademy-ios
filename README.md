# High Stakes App
- version :: 1.0.0

# How to
- Server Host: http://ec2-34-193-105-44.compute-1.amazonaws.com
- App iOS Repository: https://bitbucket.org/\_tiagoaguiar\_/hsacademy-ios
- App Android Repository: https://bitbucket.org/\_tiagoaguiar\_/hsacademyapp
- Server Repository: https://bitbucket.org/\_tiagoaguiar\_/highstakes-ws
- Video to show a little bit about how the app works: https://drive.google.com/file/d/0B79vvZnRjg1FTnNfOXRsaGtQcXM/view?usp=sharing

# Notes
- This app use cocoapods
- Bridging Objective-C with Swift
- Swift 3
- iOS 9.+

# Features Not Implemented in iOS
- Login
- Logout
- Youtube Player
