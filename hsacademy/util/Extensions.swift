//
//  Extensions.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func getUser() -> UserAccount? {
        return nil
    }
    
    func configureTopBar() {
        if let nav = navigationController {
            nav.navigationBar.barTintColor = Colors.fromHex(Constants.primaryColorDark)
            nav.navigationBar.tintColor = UIColor.white
            nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        }
    }
    
    func configureMenuTo(_ btnMenu: UIBarButtonItem) {
        self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func showController(_ nav: UINavigationController) {
        self.present(nav, animated: true, completion: nil)
    }
    
    func disposeController(_ vc: UIViewController) {
        vc.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}

extension UIView {
    
    func roundBorders(roundedRect: CGRect, cornerRadius: CGFloat) {
        let path = UIBezierPath(roundedRect: roundedRect, cornerRadius: cornerRadius)
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func shadows(_ color: UIColor, offset: CGSize, opacity: Float, radius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
}

extension UIImage {
    
    func imageWithColor(_ tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        tintColor.setFill()
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

extension B68UIFloatLabelTextField {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
    
}

extension UIScrollView {
    
    func addHeight(_ height: Int) {
        self.contentSize = CGSize(width: self.frame.size.width, height: self.frame.size.width + CGFloat(height));
    }
    
    func animateToBottom(_ offset: CGFloat = 0) {
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height - offset)
        self.setContentOffset(bottomOffset, animated: true)
    }
    
}
