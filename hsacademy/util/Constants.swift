//
//  Constants.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

struct Constants {
    
    public static var section01 = "Imóveis"
    public static var section02 = "Connect"
    public static var section03 = "novo"
    public static var dashboard = "Dashboard"
    public static var keyToken = "keyToken3"
    public static var logout = "Sair"
    
    
    public static var primaryColor: UInt32 = 0x111111
    public static var primaryColorDark: UInt32 = 0x000000
    public static var secondaryColor: UInt32 = 0xBE071A
    public static var headerTextColor: UInt32 = 0xffffff
    public static var bgHeaderTextColor: UInt32 = 0xffffff
    public static var bgHeaderColor: UInt32 = 0x387de3
    
}
