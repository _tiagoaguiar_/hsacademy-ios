//
//  Builder.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

protocol Builder {

    associatedtype T

    func build() -> T

}
