//
//  ResponseError.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

open class ResponseError {

    let responseType: ResponseType
    let description: String

    public init(responseType: ResponseType, description: String) {
        self.responseType = responseType
        self.description = description
    }

}
