//
//  HighStakesWS.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire

open class HighStakesWS {

    fileprivate static let INSTANCE: HighStakesWS = HighStakesWS()

    fileprivate let baseUrl: String = "http://ec2-34-193-105-44.compute-1.amazonaws.com"

    open static func getInstance() -> HighStakesWS {
        return INSTANCE;
    }

    open func getBaseUrl() -> String {
        return baseUrl
    }

    func getFromUrl(_ stringResponse: Bool?, url: String, callback: @escaping (AnyObject?, ResponseError?) -> Void) {
        if let stringResponse = stringResponse {
            if stringResponse {

                Alamofire.request(url).responseString { response in

                    if let resp = response.response {
                        print(resp)
                        switch(resp.statusCode) {
                        case 400:
                            callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "malformed url\(url)"))
                            break
                        case 401:
                            callback(nil, ResponseError(responseType: ResponseType.Unauthorized, description: "Usuário não autenticado"))
                            break
                        case 403:
                            callback(nil, ResponseError(responseType: ResponseType.Forbidden, description: "Ação proibida"))
                            break
                        case 200:
                            if let value = response.result.value {
                                callback(value as AnyObject?, nil)
                            } else {
                                callback(ResponseType.OK.message as AnyObject?, nil)
                            }
                            break
                        case 204:
                            callback(nil, ResponseError(responseType: ResponseType.AdNotFound, description: "Ad not found"))
                        default:
                            break
                        }
                    }

                }
            }
        } else {
            Alamofire.request(url).responseJSON { response in
                if let resp = response.response {
                    print(resp)
                    switch(resp.statusCode) {
                    case 400:
                        callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "malformed url\(url)"))
                        break
                    case 401:
                        callback(nil, ResponseError(responseType: ResponseType.Unauthorized, description: "Usuário não autenticado"))
                        break
                    case 403:
                        callback(nil, ResponseError(responseType: ResponseType.Forbidden, description: "Ação proibida"))
                        break
                    case 200:
                        if let value = response.result.value {
                            callback(value as AnyObject?, nil)
                        } else {
                            callback(ResponseType.OK.message as AnyObject?, nil)
                        }
                    default:
                        break
                    }

                }
            }
        }
    }

    func postToUrl(_ stringResponse: Bool?, url: String, parameters: [String : AnyObject], callback: @escaping (AnyObject?, ResponseError?) -> Void) {
        if let stringResponse = stringResponse {
            if stringResponse {
                let headers: HTTPHeaders = [
                    "Accept": "application/json"
                ]
                Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString { response in
                //Alamofire.request(.POST, url, parameters: parameters, encoding: ParameterEncoding.URL).responseString { response in

                    if let resp = response.response {
                        print(resp)

                        switch(resp.statusCode) {
                        case 400:
                            callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "malformed url\(url)"))
                            break
                        case 401:
                            callback(nil, ResponseError(responseType: ResponseType.Unauthorized, description: "Usuário não autenticado"))
                            break
                        case 500:
                            callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "Erro no servidor"))
                            break
                        case 200:
                            callback(response.result.value as AnyObject?, nil)
                        default:
                            break
                        }

                    }
                }
            }
        } else {
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in

                if let resp = response.response {
                    print(resp)

                    switch(resp.statusCode) {
                    case 400:
                        callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "malformed url\(url)"))
                        break
                    case 401:
                        callback(nil, ResponseError(responseType: ResponseType.Unauthorized, description: "Usuário não autenticado"))
                        break
                    case 200:
                        callback(response.result.value  as AnyObject?, nil)
                    default:
                        break
                    }

                }
            }
        }

    }

    func deleteFromUrl(_ stringResponse: Bool?, url: String, callback: @escaping (AnyObject?, ResponseError?) -> Void) {
        if let stringResponse = stringResponse {
            if stringResponse {
                Alamofire.request(url, method: .delete).responseString { response in

                    if let resp = response.response {
                        print(resp)
                        switch(resp.statusCode) {
                        case 400:
                            callback(nil, ResponseError(responseType: ResponseType.BadRequest, description: "malformed url\(url)"))
                            break
                        case 200:
                            callback(response.result.value  as AnyObject?, nil)
                        default:
                            break
                        }
                    }
                }
            }
        }
    }

    fileprivate init() {}

}
