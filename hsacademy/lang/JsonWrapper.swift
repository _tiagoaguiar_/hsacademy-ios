//
//  JSONWrapper.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class JsonWrapper {

    fileprivate let json: NSDictionary

    init(_ json: NSDictionary) {
        self.json = json
    }

    func getIntNullable(_ element: String) -> Int? {
        let value = self.json[element]
        if  value != nil {
            if ((value as? Int) != nil) {
                return value as? Int
            }
        }
        return nil
    }

    func getInt(_ element: String) -> Int {
        var response: Int = 0
        let value = self.json[element]
        if  value != nil {
            if ((value as? Int) != nil) {
                response = value as! Int
            }
        }
        return response
    }

    func getUInt(_ element: String) -> UInt {
        var response: UInt = 0
        let value = self.json[element]
        if  value != nil {
            if ((value as? UInt) != nil) {
                response = value as! UInt
            }
        }
        return response
    }

    func getUInt64(_ element: String) -> UInt64 {
        var response: UInt64 = 0
        let value = self.json[element]
        if  value != nil {
            if ((value as? UInt64) != nil) {
                response = value as! UInt64
            }
        }
        return response
    }

    func getString(_ element: String) -> String {
        var response: String = ""
        let value = self.json[element]
        if  value != nil {
            if ((value as? String) != nil) {
                response = value as! String
            }
        }
        return response
    }

    func getStringNullable(_ element: String) -> String? {
        let value = self.json[element]
        if  value != nil {
            if ((value as? String) != nil) {
                return value as? String
            }
        }
        return nil
    }

    func getDouble(_ element: String) -> Double {
        var response: Double = 0.0
        let value = self.json[element]
        if  value != nil {
            if let value = value as? Double {
                response = value
            } else {
                response = 0
            }
        }
        return response
    }

    func getError(_ element: String) -> ResponseError? {
        return self.json[element] as? ResponseError
    }

    func getArray(_ element: String) -> [String] {
        return self.json[element] as! [String]
    }

    func getArrayNullable(_ element: String) -> [String]? {
        let array = self.json[element] as? [String]
        return array != nil ? array : nil
    }

    func getIntArray(_ element: String) -> [Int] {
        return self.json[element] as! [Int]
    }

    func getIntArrayNullable(_ element: String) -> [Int]? {
        return self.json[element] as? [Int]
    }

    func getUInt64Array(_ element: String) -> [UInt64] {
        let ints = self.json[element] as! [UInt]
        var res = [UInt64]()
        for i in ints {
            res.append(UInt64(i))
        }
        return res
    }

    func getUInt64ArrayNullable(_ element: String) -> [UInt64]? {
        let ints = self.json[element] as? [UInt]
        if ints != nil {
            var res = [UInt64]()
            for i in ints! {
                res.append(UInt64(i))
            }
            return res
        }
        return nil
    }


    func getNSArray(_ element: String) -> NSArray {
        return self.json[element] as! NSArray
    }

    func getBool(_ element: String) -> Bool {
        if let integer = self.json[element] as? Int {
            return integer == 1 ? true : false
        } else if let boolean = self.json[element] as? Bool {
           return boolean
        }
        return false
    }

    func getBoolNullable(_ element: String) -> Bool? {
        let value = self.json[element]
        if value != nil {
           return self.json[element] as? Bool
        }
        return nil
    }

    func getNSObject(_ element: String) -> NSDictionary {
        return self.json[element] as! NSDictionary
    }

    func getObject(_ element: String) -> JsonWrapper {
        return JsonWrapper(self.json[element] as! NSDictionary)
    }

}
