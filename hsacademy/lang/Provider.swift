//
//  Provider.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation
import Alamofire

enum ResponseMimeType: Int {
    case xml = 0
    case json = 1
}

class Provider<R: JsonLoader> {

    typealias ElementType = R

    fileprivate let service: ServiceDelegate!

    fileprivate var url: String = ""
    fileprivate var parameters: [String : AnyObject]?
    fileprivate var method: Int = 0
    fileprivate var stringReponse: Bool?
    fileprivate var responseType = ResponseMimeType.json

    init(_ service: ServiceDelegate) {
        self.service = service
    }

    func getFromAbsolute(_ path: String, stringReponse: Bool? = nil) -> Provider {
        self.url = path
        self.method = 0
        self.stringReponse = stringReponse
        return self
    }

    func getFromAbsolute(_ path: String, responseType: ResponseMimeType = ResponseMimeType.json) -> Provider {
        self.url = path
        self.method = 3
        self.responseType = responseType
        return self
    }

    func get(_ path: String, stringReponse: Bool? = nil) -> Provider {
        self.url = HighStakesWS.getInstance().getBaseUrl() + path
        self.method = 0
        self.stringReponse = stringReponse
        return self
    }

    func post(_ path: String, stringReponse: Bool? = nil) -> Provider {
        self.url = HighStakesWS.getInstance().getBaseUrl() + path
        self.method = 1
        self.stringReponse = stringReponse
        return self
    }

    func postFromAbsolute(_ path: String, stringReponse: Bool? = nil) -> Provider {
        self.url = path
        self.method = 1
        self.stringReponse = stringReponse
        return self
    }

    func delete(_ path: String, stringReponse: Bool? = nil) -> Provider {
        self.url = HighStakesWS.getInstance().getBaseUrl() + path
        self.method = 2
        self.stringReponse = stringReponse
        return self
    }

    func with(_ param: String) -> Provider {
        if !self.url.contains("?") {
            self.url += "?\(param)"
        } else {
            self.url += "&\(param)"
        }
        return self
    }

    func with(_ param: String, _ value: AnyObject?) -> Provider {
        if value != nil {
            if !self.url.contains("?") {
                self.url += "?\(param)=\(value!)"
            } else {
                self.url += "&\(param)=\(value!)"
            }
        }

        return self
    }

    func with(_ parameters: [String : AnyObject]) -> Provider {
        self.parameters = parameters
        return self
    }

    func loadIn(_ loader: ElementType) -> ElementType {
        let callback: (AnyObject?, ResponseError?) -> Void = { (data, error) -> Void in

            if let json = data {
                if let jsonObject = json as? NSDictionary {
                    loader.load(jsonObject)
                } else {
                    loader.load(["response" : json])
                }
            } else if let error = error {
                print(error)
                loader.load(["error" : error])
            }
            self.service.notify()
        }

        let callbackArray: (AnyObject?, ResponseError?) -> Void = { (data, error) -> Void in

            if let json = data {
                if let jsonArray = json as? NSArray {
                    loader.load(["response" : jsonArray])
                    print(jsonArray)
                } else {
                    loader.load(["response" : json])
                }
            } else if let error = error {
                print(error)
                loader.load(["error" : error])
            }
            self.service.notify()
        }

        switch method {
            case 0: HighStakesWS.getInstance().getFromUrl(stringReponse, url: url, callback: callback); break
            case 1: HighStakesWS.getInstance().postToUrl(stringReponse, url: url, parameters: parameters!, callback: callback); break
            case 2: HighStakesWS.getInstance().deleteFromUrl(stringReponse, url: url, callback: callback); break
            case 3: HighStakesWS.getInstance().getFromUrl(stringReponse, url: url, callback: callbackArray); break
            default: break
        }

        return loader
    }

}
