//
//  ResponseType.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

public enum ResponseType: String {

    case BadRequest = "Solicitação inválida."
    case Unauthorized = "É necessário autenticação."
    case Forbidden = "Ação proibida."
    case AdNotFound = "Ad not found."
    case OK = "success"

    public var message: String {
        return self.rawValue
    }

}
