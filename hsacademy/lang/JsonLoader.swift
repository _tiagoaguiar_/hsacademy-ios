//
//  JSONLoader.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

protocol JsonLoader {

    associatedtype R

    func load(_ json: NSDictionary)

}
