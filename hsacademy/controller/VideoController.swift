//
//  VideoController.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class VideoController: UITableViewController {
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    // MARK: - Lifecycle Views
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTopBar()
        configureMenuTo(btnMenu)
        
        let defaults = UserDefaults.standard
        if (defaults.string(forKey: Constants.keyToken) == nil) {
            let nav = self.storyboard!.instantiateViewController(withIdentifier: "LoginID") as! UINavigationController
            self.revealViewController().setFront(nav, animated: true)
        }
    }
    
}
