//
//  LoginController.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class LoginController: UIViewController {

    @IBOutlet weak var emailTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var passwordTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTopBar()
        
        // PUT COLOR IN BACKGROUND BUTTON
        let img = CIImage(cgImage: loginButton.backgroundImage(for: UIControlState())!.cgImage!)
        
        // adding filter coloring
        let filter = CIFilter(name: "CIConstantColorGenerator")
        let color = CIColor(color: Colors.fromHex(Constants.secondaryColor))
        filter?.setValue(color, forKey: "inputColor")
        let colorImage = filter?.value(forKey: "outputImage")
        
        // adding filter multiplying
        let filterm = CIFilter(name: "CIMultiplyCompositing")
        filterm?.setValue(colorImage, forKey: "inputImage")
        filterm?.setValue(img, forKey: "inputBackgroundImage")
        let outputImage = filterm?.value(forKey: "outputImage")
        
        let endImage = UIImage.init(ciImage: outputImage! as! CIImage)
        loginButton.setBackgroundImage(endImage, for: UIControlState())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let defaults = UserDefaults.standard
        if (defaults.string(forKey: Constants.keyToken) != nil) {
            let nav = self.storyboard!.instantiateViewController(withIdentifier: "tabBarID") as! UITabBarController
            self.revealViewController().setFront(nav, animated: true)
        }
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
    }

    @IBAction func signUpAction(_ sender: UIButton) {
        let nav = self.storyboard!.instantiateViewController(withIdentifier: "SignupID") as! UINavigationController
        showController(nav)
    }
    
}
