//
//  SignUpController.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class SignUpController: UIViewController, UITextFieldDelegate, UserAccountDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var lastnameTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var emailTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var passwordTextField: B68UIFloatLabelTextField!
    @IBOutlet weak var signupButton: UIButton!
    
    // Mark: - LifeCycle Views
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTopBar()
        configureView()
    }
    
    fileprivate func configureView() {
        self.imageView.image = UIImage(named: "ic_splash")
        self.nameTextField.delegate = self
        self.lastnameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        // PUT COLOR IN BACKGROUND BUTTON
        let img = CIImage(cgImage: signupButton.backgroundImage(for: UIControlState())!.cgImage!)
        
        // adding filter coloring
        let filter = CIFilter(name: "CIConstantColorGenerator")
        let color = CIColor(color: Colors.fromHex(Constants.secondaryColor))
        filter?.setValue(color, forKey: "inputColor")
        let colorImage = filter?.value(forKey: "outputImage")
        
        // adding filter multiplying
        let filterm = CIFilter(name: "CIMultiplyCompositing")
        filterm?.setValue(colorImage, forKey: "inputImage")
        filterm?.setValue(img, forKey: "inputBackgroundImage")
        let outputImage = filterm?.value(forKey: "outputImage")
        
        let endImage = UIImage.init(ciImage: outputImage! as! CIImage)
        signupButton.setBackgroundImage(endImage, for: UIControlState())
    }
    
    // MARK: - Text Field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string == " " ? false : true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.scrollView.addHeight(250)
        self.scrollView.animateToBottom()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.scrollView.addHeight(0)
        return true
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        disposeController(self)
    }
    
    @IBAction func signupAction(_ sender: UIButton) {
        if emailTextField.text!.isEmpty
            || nameTextField.text!.isEmpty
            || passwordTextField.text!.isEmpty
            || lastnameTextField.text!.isEmpty {
            let alert = UIAlertView.init(title: nil,
                                         message: "Todos os campos devem ser preenchidos.",
                                         delegate: nil,
                                         cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        
        if !emailTextField.isValidEmail() {
            let alert = UIAlertView.init(title: nil,
                                         message: "O email não é válido.",
                                         delegate: nil,
                                         cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        
        UserAccountService(self).insertUser(name: nameTextField.text!,
                                            lastname: lastnameTextField.text!,
                                            email: emailTextField.text!,
                                            password: passwordTextField.text!)
    }
    
    // MARK - Requests
    
    func onLoadData(_ response: String) {
        ProgressBar.hide(view)
        if response == ResponseType.Unauthorized.message {
            let alert = UIAlertView.init(title: nil,
                                         message: "Usuário Já Existente.",
                                         delegate: nil,
                                         cancelButtonTitle: "Ok")
            
            alert.show()
            return
        }
        
        if !response.isEmpty {
            self.disposeController(self)
            let defaults = UserDefaults.standard
            if (defaults.string(forKey: Constants.keyToken) == nil) {
                defaults.setValue(response, forKey: Constants.keyToken)
            }
        }
    }
    
}
