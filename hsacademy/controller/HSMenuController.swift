//
//  HSMenuController.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class HSMenuController: UITableViewController, UserAccountDelegate {
    
    var section: Array<String>?
    var items: Array<Array<String>?>?
    var imageView: UIImageView?
    var icLogoView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildTableHeaderView()
        
        section = [Constants.section01, Constants.section02, Constants.section03]
        
        var listing = Array<String>()
        
        listing.append(Constants.dashboard)
        
        
        var connect = Array<String>()
        connect.append(Constants.section02)
        
        var app = Array<String>()
        app.append(Constants.section03)
        app.append(Constants.logout)
        
        items = Array()
        items!.append(listing)
        items!.append(connect)
        items!.append(app)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.bounces = (scrollView.contentOffset.y > 0)
    }
    
    // MARK: Defaults
    
    fileprivate func buildTableHeaderView() {
        let bgHeader = UIImage(named: "ic_header", in: Bundle(for: HSMenuController.self), compatibleWith: nil)
        let icLogo = UIImage(named: "ic_splash")
        let img = CIImage(cgImage: bgHeader!.cgImage!)
        
        // adding filter coloring
        let filter = CIFilter(name: "CIConstantColorGenerator")
        let color = CIColor(color: Colors.fromHex(Constants.primaryColor))
        filter?.setValue(color, forKey: "inputColor")
        let colorImage = filter?.value(forKey: "outputImage")
        
        // adding filter multiplying
        let filterm = CIFilter(name: "CIMultiplyCompositing")
        filterm?.setValue(colorImage, forKey: "inputImage")
        filterm?.setValue(img, forKey: "inputBackgroundImage")
        let outputImage = filterm?.value(forKey: "outputImage")
        
        let endImage = UIImage.init(ciImage: outputImage! as! CIImage)
        
        let x: CGFloat = 0
        let y: CGFloat = -UIApplication.shared.statusBarFrame.height // status bar height
        let width: CGFloat = tableView.frame.size.width // table width
        
        let offset: CGFloat = tableView.bounds.width <= 320
            ? 20.0 : tableView.bounds.width <= 767
            ? 60.0 : tableView.bounds.width <= 1023
            ? 270.0 : 420.0

        imageView = UIImageView(frame: CGRect(x: x, y: y, width: width, height: ((9/16) * width - offset) ))
        imageView!.image = endImage
        imageView!.clipsToBounds = true
        imageView!.contentMode = UIViewContentMode.scaleToFill
        
        icLogoView = UIImageView(frame: CGRect(x: (self.revealViewController().rearViewRevealWidth / 2) - (icLogo!.size.width / 2),
                                               y: UIApplication.shared.statusBarFrame.height, width: icLogo!.size.width,
                                               height: icLogo!.size.height))
        icLogoView!.image = icLogo
    }
    
    // MARK: Table Delegates
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section![section]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.section!.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return imageView!.frame.size.height + imageView!.frame.origin.y // image height - status bar difference
        }
        return 30
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items![section]!.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let view = UIView.init(frame: CGRect(x: 0, y: -UIApplication.shared.statusBarFrame.height,
                                                 width: tableView.frame.size.width, height: imageView!.frame.size.height))
            let label = UILabel.init(frame: CGRect(x: 10, y: (view.frame.size.height + view.frame.origin.y) - 20,
                                                   width: tableView.frame.size.width, height: 18))
            
            label.font = UIFont.boldSystemFont(ofSize: 12)
            if let user = getUser() {
                label.text = user.email
            }
            label.textColor = Colors.fromHex(Constants.bgHeaderTextColor)
            view.addSubview(label)
            
            view.addSubview(imageView!)
            view.sendSubview(toBack: imageView!)
            view.addSubview(icLogoView!)
            
            return view
        }
        
        let label = UILabel.init(frame: CGRect(x: 20, y: 4, width: 320, height: 20))
        label.font = UIFont.boldSystemFont(ofSize: 16);
        label.textColor = Colors.fromHex(0xc9c9c9)
        label.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        let view = UIView()
        view.backgroundColor = Colors.fromHex(0xf6f6f6)
        view.addSubview(label)
        
        return view;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = self.items![(indexPath as NSIndexPath).section]![(indexPath as NSIndexPath).row]

        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = self.items![(indexPath as NSIndexPath).section]![(indexPath as NSIndexPath).row]
        cell.textLabel?.highlightedTextColor = Colors.fromHex(Constants.secondaryColor)
        
        let img = cell.imageView?.image;
        cell.imageView?.image = img!.imageWithColor(Colors.fromHex(0x757575)).withRenderingMode(.alwaysOriginal)
        cell.imageView?.highlightedImage = img!.imageWithColor(Colors.fromHex(Constants.secondaryColor)).withRenderingMode(.alwaysOriginal)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 2 && (indexPath as NSIndexPath).row == 1 {
            let defaults = UserDefaults.standard
            if let token = (defaults.string(forKey: Constants.keyToken)) {
                UserAccountService(self).logout(token: token)
            }
            
            
            
        }
        // revealViewController().revealToggle(animated: true)
    }
    
    func onLoadData(_ response: String) {
        if !response.isEmpty {
            disposeController(self)
            let nav = self.storyboard!.instantiateViewController(withIdentifier: "LoginID") as! UINavigationController
            showController(nav)
        }
    }
    
}
