//
//  User.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

@objc protocol User {

    func getId() -> Int
    func getEmail() -> String
    func getName() -> String


}
