//
//  UserAccountLoader.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class UserAccountLoader: JsonLoader {

    typealias R = String

    var data: String?
    var user: User?

    func load(_ json: NSDictionary) {
        let obj = JsonWrapper(json)

        if let error = obj.getError("error") {
            if error.responseType == ResponseType.Unauthorized || error.responseType == ResponseType.Forbidden {
                data = error.responseType.message
                return
            }
        }
        else if let obj = json["data"] as? NSDictionary {
            let jsonWrapper = JsonWrapper(obj)
            self.user = Loader(jsonWrapper).build()
        }

        else if let obj = json["response"] as? NSDictionary {
            let jsonWrapper = JsonWrapper(obj)
            data = jsonWrapper.getString("token")
        }

        else {
            data = obj.getString("token")
        }
    }

    fileprivate class Loader: User, Builder {

        typealias T = User
        fileprivate let json: JsonWrapper

        init(_ json: JsonWrapper) {
            self.json = json
        }

        fileprivate func build() -> User {
            return UserImpl(builder: self)
        }

        @objc fileprivate func getId() -> Int {
            return json.getInt("id")
        }

        @objc fileprivate func getEmail() -> String {
            return json.getString("email")
        }

        @objc fileprivate func getName() -> String {
            return json.getString("name")
        }

    }

}
