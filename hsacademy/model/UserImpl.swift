//
//  UserImpl.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

@objc class UserImpl: NSObject, User {

    fileprivate let id: Int
    fileprivate let email: String
    fileprivate let name: String

    init(builder: User) {
        self.id = builder.getId()
        self.email = builder.getEmail()
        self.name = builder.getName()
    }

    func getId() -> Int {
        return id
    }

    func getEmail() -> String {
        return email
    }

    func getName() -> String {
        return name
    }

}
