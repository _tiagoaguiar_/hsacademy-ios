//
//  ProgressBar.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

class ProgressBar: NSObject {

    fileprivate static func build() -> UIImageView {
        let statusImage = UIImage(named: "status-19", in: Bundle(for: ProgressBar.self), compatibleWith: nil)
        let activityImageView = UIImageView.init(image: statusImage)

        activityImageView.animationImages = [
            UIImage(named: "status-18", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-17", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-16", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-15", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-14", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-13", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-12", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-11", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-10", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-9", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-8", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-7", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-6", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-5", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-4", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-3", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-2", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-1", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
            UIImage(named: "status-0", in: Bundle(for: ProgressBar.self), compatibleWith: nil)!,
        ]

        activityImageView.animationDuration = 0.4;
        activityImageView.startAnimating()
        return activityImageView
    }

    static func show(_ view: UIView) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.show(animated: true)
        hud.customView = build()
        hud.minShowTime = 1
        hud.shadows(UIColor.lightGray, offset: CGSize(width: 1, height: 1), opacity: 1, radius: 10.0)
        hud.bezelView.style = MBProgressHUDBackgroundStyle.solidColor
        hud.bezelView.color = UIColor.white
        hud.mode = MBProgressHUDMode.customView
    }

    static func hide(_ view: UIView) {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }

}
