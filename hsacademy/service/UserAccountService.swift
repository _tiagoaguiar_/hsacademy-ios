//
//  UserAccountService.swift
//  hsacademy
//
//  Created by Tiago Aguiar on 19/12/16.
//  Copyright © 2016 HS Academy. All rights reserved.
//

import Foundation

@objc protocol UserAccountDelegate {
    func onLoadData(_ response: String)
    @objc optional func onLoadUser(_ user: User)
}

class UserAccountService: NSObject, ServiceDelegate {

    var userAccountLoader: UserAccountLoader?
    var userAccountDelegate: UserAccountDelegate!

    init( _ userAccountDelegate: UserAccountDelegate ) {
        self.userAccountDelegate = userAccountDelegate
    }

    func findUser(token: String) {
        userAccountLoader = Provider(self)
            .get("/rest/userSession")
            .with("authToken", token as AnyObject?)
            .loadIn(UserAccountLoader())
    }

    func login(email: String, password: String) {
        let params: [String : AnyObject] = [
            "email" : email as AnyObject,
            "password" : password as AnyObject
        ]
        userAccountLoader = Provider(self)
            .post("/rest/userAccount")
            .with(params)
            .loadIn(UserAccountLoader())
    }

    func logout(token: String) {
        userAccountLoader = Provider(self)
            .delete("/rest/userSession", stringReponse: true)
            .with("token", token as AnyObject?)
            .loadIn(UserAccountLoader())
    }

    func insertUser(name: String, lastname: String, email: String, password: String) {
        let params: [String : AnyObject] = [
            "name" : name as AnyObject,
            "lastname" : lastname as AnyObject,
            "email" : email as AnyObject,
            "password" : password as AnyObject
        ]
        userAccountLoader = Provider(self)
            .post("/rest/userAccount")
            .with(params)
            .loadIn(UserAccountLoader())
    }

    func notify() {
        if let loader = userAccountLoader {
            if let data = loader.data {
                userAccountDelegate!.onLoadData(data)
            } else if let user = loader.user {
                userAccountDelegate!.onLoadUser!(user)
            }
        }
    }

}
